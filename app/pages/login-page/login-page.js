{
    const CellsPage = customElements.get('cells-page');
  
    class LoginPage extends CellsPage {
      static get is() {
        return 'login-page';
      }
  
      static get properties() {
        return {
          // NOT NEEDED IF YOU INHERIT FROM CellsPage
          params: {
            type: Object,
            value: {}
          }
        };
      }

      _requestGrantingTicket(dataLogin){
          this.$.dmLogin.requestGrantingTicket(dataLogin);
      }

      _loginSuccess(){
        this.navigate('globalposition');
      }

    }
  
    window.customElements.define(LoginPage.is, LoginPage);
  }