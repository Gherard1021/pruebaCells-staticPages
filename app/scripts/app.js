(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'globalposition': '/globalposition'
    }
  });

}(document));
