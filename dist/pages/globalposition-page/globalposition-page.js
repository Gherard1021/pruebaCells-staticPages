{
    const CellsPage = customElements.get('cells-page');
  
    class GlobalPosition extends CellsPage {
      static get is() {
        return 'globalposition-page';
      }
  
      static get properties() {
        return {
          // NOT NEEDED IF YOU INHERIT FROM CellsPage
          params: {
            type: Object,
            value: {}
          }
        };
      }

      ready(){
          super.ready();
          document.getE.remove();
      }
    }
  
    window.customElements.define(GlobalPosition.is, GlobalPosition);
  }